<?php

namespace Drupal\yamlelement\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Plugin implementation of the 'Yaml' formatter.
 *
 * @FieldFormatter(
 *   id = "yamlelement",
 *   label = @Translation("Yaml"),
 *   field_types = {
 *     "map"
 *   }
 * )
 */
class YamlFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      // WTF: https://www.drupal.org/node/2155247
      unset($item->_attributes);

      $value = $item::mainPropertyName() ? $item->get($item::mainPropertyName()) : $item->getValue();
      $yaml = Yaml::dump($value, 999, 2, Yaml::DUMP_OBJECT | Yaml::DUMP_EXCEPTION_ON_INVALID_TYPE | Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK);
      //$yaml = var_export($value, TRUE);
      $element[$delta] = [
        '#markup' => $yaml,
        '#prefix' => '<pre>',
        '#suffix' => '</pre>',
      ];
    }

    return $element;
  }

}
