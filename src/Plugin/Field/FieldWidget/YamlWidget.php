<?php

namespace Drupal\yamlelement\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'yamlelement' field widget.
 *
 * @FieldWidget(
 *   id = "yamlelement",
 *   label = @Translation("Yaml"),
 *   field_types = {"map"},
 * )
 */
class YamlWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // @todo Improve this.
    if (!$items->count()) {
      $items->appendItem();
    }
    /** @var \Drupal\Core\Field\FieldItemInterface $item */
    foreach ($items as $i => $item) {
      if ($item::mainPropertyName()) {
        $element[$i][$item::mainPropertyName()] = [
          '#type' => 'yaml',
          '#allow_objects' => TRUE,
          '#default_value' => $item->get($item::mainPropertyName()),
        ];
      }
      else {
        $element[$i] = [
          '#type' => 'yaml',
          '#allow_objects' => TRUE,
          '#default_value' => $item->getValue(),
        ];
      }
    }
    return $element;
  }

}
