<?php

namespace Drupal\yamlelement\Element;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Textarea;
use Drupal\Core\Security\TrustedCallbackInterface;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml as SymfonyYaml;

/**
 * Provides a form element for yaml input via textarea.
 *
 * @FormElement("yaml")
 */
class Yaml extends Textarea implements TrustedCallbackInterface {

  /**
   * @inheritDoc
   */
  public function getInfo() {
    $info = parent::getInfo();
    $info['#allow_objects'] = FALSE;
    $info['#element_validate'][] = [static::class, 'validateYaml'];
    $info['#pre_render'][] = [static::class, 'preRenderYaml'];
    return $info;
  }

  /**
   * Element #pre_render callback.
   */
  public static function preRenderYaml($element) {
    $flags = SymfonyYaml::DUMP_EXCEPTION_ON_INVALID_TYPE | SymfonyYaml::DUMP_MULTI_LINE_LITERAL_BLOCK;
    if (!empty($element['#allow_objects'])) {
      $flags |= SymfonyYaml::DUMP_OBJECT;
    }
    $element['#value'] = SymfonyYaml::dump($element['#value'], 999, 2, $flags);
    return $element;
  }

  /**
   * Element element_validate callback.
   */
  public static function validateYaml($element, FormStateInterface $form_state, $form) {
    $input = $form_state->getValue($element['#parents']);
    try {
      $flags = SymfonyYaml::PARSE_EXCEPTION_ON_INVALID_TYPE;
      if (!empty($element['#allow_objects'])) {
        $flags |= SymfonyYaml::PARSE_OBJECT;
      }
      $value = SymfonyYaml::parse($input, $flags);
    }
    catch (ParseException $e) {
      $form_state->setError($element, t('The Yaml in %field is not valid.', ['%field' => $element['#title']]));
    }
    if (isset($value)) {
      $form_state->setValue($element['#parents'], $value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRenderYaml'];
  }


}
